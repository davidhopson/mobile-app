import { NgModule } from '@angular/core'
import { Routes } from '@angular/router'
import { NativeScriptRouterModule, NSEmptyOutletComponent } from '@nativescript/angular'
import { ItemDetailComponent } from './item-detail/item-detail.component'

const routes: Routes = [
    {
        path: '',
        redirectTo: '/(homeTab:home/default//searchTab:search/default)',
        pathMatch: 'full',
    },

    {
        path: 'home',
        component: NSEmptyOutletComponent,
        loadChildren: () => import('~/app/home/home.module').then((m) => m.HomeModule),
        outlet: 'homeTab',
    },
    {
        path: 'search',
        component: NSEmptyOutletComponent,
        loadChildren: () => import('~/app/search/search.module').then((m) => m.SearchModule),
        outlet: 'searchTab',
    },
    {
        path: 'item',
        loadChildren: () => import('~/app/item-detail/item-detail.module').then((m) => m.ItemDetailModule),
        component: ItemDetailComponent
    }
]

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule],
})
export class AppRoutingModule { }
