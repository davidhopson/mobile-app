import { Component, OnInit, ViewContainerRef } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { ModalDialogOptions, ModalDialogService, RouterExtensions } from '@nativescript/angular';
import { EventData, Http, ListPicker } from '@nativescript/core';
import { of } from 'rxjs';
import { ModalComponent } from '../modal/modal.component';

import { DataService, DataItem } from '../shared/data.service'

@Component({
	selector: 'Home',
    providers: [ModalDialogService],
	templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {
	items: any;
    type: string = "character";

	constructor(private _itemService: DataService, private _router: RouterExtensions, private modalService: ModalDialogService, private viewContainerRef: ViewContainerRef) { }

	ngOnInit(): void {
        this.loadData();
	}

    nav() {
        console.log("tap");
        this._router.navigate(["/item"]);
    }

    loadData = () => {
        this._itemService.getAll(this.type).then((res: any) => {
            this.items = res.data;
        });
    }

    showModal() {
        const options: ModalDialogOptions = {
            viewContainerRef: this.viewContainerRef,
            fullscreen: true,
            context: {}
        }

        this.modalService.showModal(ModalComponent, options).then((res: string) => {
            this.type = res === "Species" ? res.toLowerCase() : res.toLowerCase().substr(0, res.length - 1);
            this.loadData();
        });
    }
}
