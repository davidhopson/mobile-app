import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core'
import { NativeScriptCommonModule } from '@nativescript/angular'
import { ModalComponent } from '../modal/modal.component'

import { HomeRoutingModule } from './home-routing.module'
import { HomeComponent } from './home.component'

@NgModule({
  imports: [NativeScriptCommonModule, HomeRoutingModule],
  declarations: [HomeComponent, ModalComponent],
  schemas: [NO_ERRORS_SCHEMA],
  entryComponents: [ModalComponent]
})
export class HomeModule {}