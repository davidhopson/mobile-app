import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { RouterExtensions } from '@nativescript/angular'

import { DataService } from '../shared/data.service'

@Component({
    selector: 'ItemDetail',
    templateUrl: './item-detail.component.html',
})
export class ItemDetailComponent implements OnInit {
    item: any

    constructor(
        private _data: DataService,
        private _route: ActivatedRoute,
        private _routerExtensions: RouterExtensions
    ) { }

    ngOnInit(): void {
        const routeParams = this._route.snapshot.params;
        this._data.getItem(routeParams.type, routeParams.id).then(res => this.item = res);
    }

    onBackTap(): void {
        this._routerExtensions.back()
    }
}
