import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core'
import { NativeScriptCommonModule } from '@nativescript/angular'
import { ItemDetailComponent } from './item-detail.component'
import { ItemRoutingModule } from './item-routing.module'

@NgModule({
  imports: [NativeScriptCommonModule, ItemRoutingModule],
  declarations: [ItemDetailComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class ItemDetailModule {}