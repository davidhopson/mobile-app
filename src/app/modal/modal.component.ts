import { Component, OnInit } from "@angular/core";
import { ModalDialogParams } from "@nativescript/angular";

@Component({
    selector: "modal",
    templateUrl: "./modal.component.html"
})
export class ModalComponent implements OnInit {

    constructor(private params: ModalDialogParams) {}

    types = ["Characters", "Planets", "Species", "Starships", "Vehicles"];

    ngOnInit() {}

    close() {
        this.params.closeCallback();
    }

    onItemTap(e) {
        this.params.closeCallback(this.types[e.index]);
    }
}

