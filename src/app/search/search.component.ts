import { Component, OnInit } from '@angular/core'
import { SearchBar } from '@nativescript/core';
import { DataService } from '../shared/data.service';

@Component({
    selector: 'Search',
    templateUrl: './search.component.html',
})
export class SearchComponent implements OnInit {
    searchString: string = "";
    items: any;

    constructor(private _itemService: DataService) {
        // Use the constructor to inject services.
    }

    ngOnInit(): void {
        // Use the "ngOnInit" handler to initialize data for the view.
    }

    search = () => {
        this._itemService.search(this.searchString).then(res => this.items = res);
    }

    returnSearchBar = obj => obj as SearchBar;

    onTextChanged = (args) => {
        this.searchString = (args.object as SearchBar).text;

        if (this.searchString !== "") this.search();
    };

    onClear = args => this.searchString = "";
}
