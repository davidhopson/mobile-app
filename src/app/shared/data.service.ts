import { Injectable } from '@angular/core'
import { Http } from '@nativescript/core';

export interface DataItem {
	id: number
	name: string
	description: string
}

@Injectable({
	providedIn: 'root',
})
export class DataService {
    url = "https://b27023f74be4.ngrok.io";

	getAll = async (type) => await Http.getJSON(`${this.url}/api/${type}`);

    getItem = async (type, id) => await Http.getJSON(`${this.url}/api/${type}/${id}`);
	
    search = async (string) => await Http.getJSON(`${this.url}/api/search/${string}`);
}
